package br.ucsal.bes20182.testequalidade.bancopovo.teste;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import br.ucsal.bes20182.testequalidade.bancopovo.ContaCorrente;
import junit.framework.Assert;

public class DepositoStep {

	ContaCorrente conta;	

	@Given("que abri uma conta corrente")
	public void abrirContaCorrente() {
		conta = new ContaCorrente();

	}

	@When("realizo um deposito de 300 reais")
	public void deposito() {

		conta.depositar(300d);

	}

	@Then("o saldo da minha conta sera de 300 reais")
	public void consultarSaldo() {
		//Assert do 300 com o saldo que voc� recuperou......
		conta.consultarSaldo();
	}

}
